<?php

/*
* AÑADIR CAMPO NIF/CIF EN EL FORMULARIO DE PAGO
*/
function woo_custom_field_checkout($checkout)
{
    echo '<div id="additional_checkout_field">';
    woocommerce_form_field('nif', array( // Identificador del campo
        'type' => 'text',
        'class' => array('my-field-class form-row-wide'),
        'label' => __('NIF / CIF'),   // Nombre del campo
    ), $checkout->get_value('nif'));    // Identificador del campo
    echo '</div>';
}

add_action('woocommerce_after_checkout_billing_form', 'woo_custom_field_checkout');
/*
* INCLUYE NIF/CIF EN LOS DETALLES DEL PEDIDO CON EL NUEVO CAMPO
*/
function woo_custom_field_checkout_update_order($order_id)
{
    if (!empty($_POST['nif'])) {
        update_post_meta($order_id, 'NIF', sanitize_text_field($_POST['nif']));
    }
}

add_action('woocommerce_checkout_update_order_meta', 'woo_custom_field_checkout_update_order');
/*
* MUESTRA EL VALOR DEL CAMPO NIF/CIF LA PÁGINA DE MODIFICACIÓN DEL PEDIDO
*/
function woo_custom_field_checkout_edit_order($order)
{
    echo '<p><strong>' . __('NIF') . ':</strong> ' . get_post_meta($order->id, 'NIF', true) . '</p>';
}

add_action('woocommerce_admin_order_data_after_billing_address', 'woo_custom_field_checkout_edit_order', 10, 1);
/*
* INCLUYE EL CAMPO NIF/CIF EN EL CORREO ELECTRÓNICO DE AVISO A TU CLIENTE
*/
function woo_custom_field_checkout_email($keys)
{
    $keys[] = 'NIF';
    return $keys;
}

add_filter('woocommerce_email_order_meta_keys', 'woo_custom_field_checkout_email');

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
//numero de productos recomendados
if (!function_exists('woocommerce_upsell_display')) {

    /**
     * Output product up sells.
     *
     * @param int $limit (default: -1)
     * @param int $columns (default: 4)
     * @param string $orderby Supported values - rand, title, ID, date, modified, menu_order, price.
     * @param string $order Sort direction.
     */
    function woocommerce_upsell_display($limit = '-1', $columns = 4, $orderby = 'rand', $order = 'desc')
    {
        global $product, $woocommerce_loop;

        if (!$product) {
            return;
        }

        // Handle the legacy filter which controlled posts per page etc.
        $args = apply_filters('woocommerce_upsell_display_args', array(
            'posts_per_page' => $limit,
            'orderby' => $orderby,
            'columns' => 4,
        ));
        $woocommerce_loop['name'] = 'up-sells';
        $woocommerce_loop['columns'] = apply_filters('woocommerce_upsells_columns', isset($args['columns']) ? $args['columns'] : $columns);
        $orderby = apply_filters('woocommerce_upsells_orderby', isset($args['orderby']) ? $args['orderby'] : $orderby);
        $limit = apply_filters('woocommerce_upsells_total', isset($args['posts_per_page']) ? $args['posts_per_page'] : $limit);

        // Get visble upsells then sort them at random, then limit result set.
        $upsells = wc_products_array_orderby(array_filter(array_map('wc_get_product', $product->get_upsell_ids()), 'wc_products_array_filter_visible'), $orderby, $order);
        $upsells = $limit > 0 ? array_slice($upsells, 0, $limit) : $upsells;

        wc_get_template('single-product/up-sells.php', array(
            'upsells' => $upsells,

            // Not used now, but used in previous version of up-sells.php.
            'posts_per_page' => $limit,
            'orderby' => $orderby,
            'columns' => $columns,
        ));
    }
}

add_theme_support('wc-product-gallery-zoom');
add_theme_support('wc-product-gallery-lightbox');
add_theme_support('wc-product-gallery-slider');
add_filter('wc_product_sku_enabled', '__return_false');
/**
 * Hide shipping rates when free shipping is available.
 * Updated to support WooCommerce 2.6 Shipping Zones.
 *
 * @param array $rates Array of rates found for the package.
 * @return array
 */
function my_hide_shipping_when_free_is_available($rates)
{
    $free = array();
    foreach ($rates as $rate_id => $rate) {
        if ('free_shipping' === $rate->method_id) {
            $free[$rate_id] = $rate;
            break;
        }
    }
    return !empty($free) ? $free : $rates;
}

add_filter('woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100);

//function insert_jquery()
//{
//    wp_deregister_script('jquery');
//    wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', array(), '1.10.2', false);
//
//}

add_filter('wp_enqueue_scripts', 'insert_jquery');
function theme_cleanup()
{
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'feed_links', 2);
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
}

add_action('after_setup_theme', 'theme_cleanup', 16);
/**
 * Removes WordPress version from RSS
 */
function theme_rss_version()
{
    return '';
}

add_filter('the_generator', 'theme_rss_version');
/**
 * Removes injected CSS from recent comments widget
 */
function theme_remove_wp_widget_recent_comments_style()
{
    if (has_filter('wp_head', 'wp_widget_recent_comments_style')) {
        remove_filter('wp_head', 'wp_widget_recent_comments_style');
    }
}

add_filter('wp_head', 'theme_remove_wp_widget_recent_comments_style', 1);
/**
 * Removes injected CSS from gallery
 */
function theme_gallery_style($css)
{
    return preg_replace("!!s", '', $css);
}

add_filter('gallery_style', 'theme_gallery_style');

/**DESABILITA wp-admin-ajax***/
// function stop_heartbeat() {
// wp_deregister_script('heartbeat');
// }
//Modificar frecuencia de latido de Heartbeat API
function modificar_heartbeat($settings)
{
    $settings['interval'] = 60; //Cambia a lo que sea entre 15-60
    return $settings;
}

add_filter('heartbeat_settings', 'modificar_heartbeat');

// Disable Admin Bar for everyone
if (!function_exists('df_disable_admin_bar')) {
    function df_disable_admin_bar()
    {
        // for the admin page
        remove_action('admin_footer', 'wp_admin_bar_render', 1000);
        // for the front-end
        remove_action('wp_footer', 'wp_admin_bar_render', 1000);
        // css override for the admin page
        function remove_admin_bar_style_backend()
        {
            echo '<style>body.admin-bar #wpcontent, body.admin-bar #adminmenu { padding-top: 0px !important; }</style>';
        }

        add_filter('admin_head', 'remove_admin_bar_style_backend');

        // css override for the frontend
        function remove_admin_bar_style_frontend()
        {
            echo '<style type="text/css" media="screen">
			html { margin-top: 0px !important; }
			* html body { margin-top: 0px !important; }
			</style>';
        }

        add_filter('wp_head', 'remove_admin_bar_style_frontend', 99);
    }
}
add_action('init', 'df_disable_admin_bar');

add_action('woocommerce_checkout_order_review', 'woocommerce_order_review', 20);
add_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 10);
add_action('init', 'stop_heartbeat', 1);
//LOGO ENTRECIMAS
function logo()
{
    ?>
    <a href="<?php echo get_home_url(); ?>">
        <div class="logo">
            <img id="img-logo" src="<?php echo get_home_url(); ?>/wp-content/uploads/logo/4.png" alt="Entrecimas-logo"/>
            <img id="letras-logo" src="<?php echo get_home_url(); ?>/wp-content/uploads/logo/5.png" alt="entrecimas"/>
        </div>
    </a>
    <?php
}

add_action('storefront_header', 'logo');


if (!function_exists('storefront_handheld_footer_bar')) {
    /**
     * MENU TOP MOVIL
     * Display a menu intended for use on handheld devices.
     *
     * @since 2.0.0
     */
    function storefront_handheld_footer_bar()
    {
        $links = array(

            // 'search' => array(
            //     'priority' => 20,
            //     'callback' => 'storefront_handheld_footer_bar_search',
            // ),
            'cart' => array(
                'priority' => 30,
                'callback' => 'storefront_handheld_footer_bar_cart_link',
            ),

        );

        if (wc_get_page_id('myaccount') === -1) {
            unset($links['my-account']);
        }

        if (wc_get_page_id('cart') === -1) {
            unset($links['cart']);
        }

        $links = apply_filters('storefront_handheld_footer_bar_links', $links); ?>
        <div class="storefront-handheld-footer-bar">
            <ul class="columns-5">
                <?php foreach ($links as $key => $link) : ?>
                    <li class="<?php echo esc_attr($key); ?>">
                        <?php
                        if ($link['callback']) {
                            call_user_func($link['callback'], $key, $link);
                        } ?>
                    </li>
                <?php endforeach;
                ?>
                <li class="search">
                    <a href="#">search</a>
                    <div class="site-search input-busqueda">
                        <?php
                        echo do_shortcode('[aws_search_form]');
                        ?>
                    </div>
                </li>
                <li class="myaccount">
                    <?php

                    if (is_user_logged_in()) {
                        global $current_user;
                        get_currentuserinfo();
                        $usuario = esc_attr($current_user->user_login); ?>
                        <a class="mi-cuenta" href="<?php echo get_home_url() ?>/mi-cuenta"><?php echo $usuario ?></a>
                        <?php
                    } else {
                        ?><a class="login" href="<?php echo get_home_url() ?>/login"></a>
                        <?php
                    }
                    ?>


                </li>
                <li class="iniLogo">
                    <a href="<?php echo get_home_url(); ?>">
                        <div>
                            <img id="img-logo" src="<?php echo get_home_url(); ?>/wp-content/uploads/logo/4-min.png"
                                 alt=""/>
                        </div>
                    </a></li>


            </ul>

        </div>
        <?php

    }
}

// function engancharLoginLocial(){
// 	<div class="login-hook"> <p></p>
// 		<p>
// 			O prefiere conectar con:
// 		</p>
// 								<span class="fa fa-facebook face" style="cursor:pointer;" onclick="facebook_login()"></span>
// 								<span class="fa fa-google google" style="cursor:pointer;" onclick="google_login()"></span>
// 	</div>
//
// }
// add_action('login_form','engancharLoginLocial');
/**
 * Numero productos por fila
 * @return [num de productos por fila]
 */
function loop_columns()
{
    return 4; // 5 products per row
}

add_filter('loop_shop_columns', 'loop_columns', 999);
/**
 * numero productos por pagina: 16
 */
add_filter('loop_shop_per_page', create_function('$cols', 'return 16;'), 16);
function storefront_cart_link()
{
    ?>
    <a class="cart-contents" href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>"
       title="<?php esc_attr_e('View your shopping cart', 'storefront'); ?>">
        <span class="amount"><?php echo wp_kses_data(WC()->cart->get_cart_subtotal()); ?></span> <span class="separate">| </span><span
                class="count"><?php echo wp_kses_data(sprintf(_n('%d item', '%d items', WC()->cart->get_cart_contents_count(), 'storefront'), WC()->cart->get_cart_contents_count())); ?>
            <span>-</span></span>
    </a>
    <?php

}

// Register Navigation Menus
if (!function_exists('marabelia_menus')) {

    function marabelia_menus()
    {
        $locations = array(
            'topbarMiCuenta' => __('Menu ubicado en footer', 'text_domain'),
            'topbarRedes' => __('menu ubicado en topbar', 'text_domain'),
        );
        register_nav_menus($locations);
    }

    add_action('init', 'marabelia_menus');
}
/**
 * Añadir botones compartir en redes sociales articulo
 * @return [type] [description]
 */
function ds()
{
    $urlActual = urldecode('http://' . urlencode($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']));
    $urlFace = $urlActual; ?>
    <div class="compartir">
        <?php
        $urlActual = urldecode('http://' . urlencode($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']));
        $urlT = urlencode(get_the_title() . ' ' . $urlActual . ' #EntreCimas');
        $urlFace = $urlActual . '/&amp;t=' . get_the_title();
        //  $img_url = $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' );
        ?>
        <span class="share">Compartir: </span>
        <a title="Compartir en Whatsaap" class="fa fa-whatsapp wasas"
           href="whatsapp://send?text=<?php echo get_permalink(); ?>" data-action="share/whatsapp/share"></a>
        <a target="blank" title="Compartir en twitter" class="fa fa-twitter tuiter"
           href="http://twitter.com/home?status=<?php echo $urlT ?>"></a>
        <a target="blank" title="Compartir en google plus" class="fa fa-google g"
           href="https://plusone.google.com/_/+1/confirm?hl=es&url=<?php echo $urlActual ?>"></a>
        <a target="blank" title="Compartir en Facebook" class="fa fa-facebook face"
           href="http://www.facebook.com/sharer.php?u=<?php echo $urlFace ?>"> </a>
    </div>
    <?php

}

add_action('woocommerce_after_add_to_cart_form', 'ds');


function filtro()
{
    ?>

    <div id="filtro"><i class="fa fa-filter" aria-hidden="true"></i> Filtrar</div>
    <?php


}

add_action('woocommerce_before_shop_loop', 'filtro');


/**
 * Crear una zonan de widgets que podremos gestionar
 * fácilmente desde administrador de Wordpress.
 */
function mis_widgets()
{
    register_sidebar(
        array(
            'name' => __('Top bar header izquierda'),
            'id' => 'topleft',
            'before_widget' => '<div id="social">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
        )
    );
}

add_action('init', 'mis_widgets');
// remove default sorting dropdown in StoreFront Theme
add_action('init', 'delay_remove');

//remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',20);

/* COMIENZA SCRIPT QUITAR COMENTARIOS WOOCOMMERCE by @GiLeiva */
add_filter('woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);
function sb_woo_remove_reviews_tab($tabs)
{
    unset($tabs['reviews']);
    return $tabs;
}

/* FIN DE SCRIPT QUITAR COMENTARIOS WOOCOMMERCE by @GiLeiva */
// function delay_remove() {
// remove_action( 'woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 10 );
// remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 10 );
// }

//* Add stock status to archive pages
//function envy_stock_catalog()
//{
//    global $product;
//
//    if ($product->is_in_stock()) {
//        echo '<label class="stock" >Disponibilidad: <span>' . $product->get_stock_quantity() . __(' ¡ En Stock !', 'envy') . '</span></label>';
//    } else {
//        echo '<label class="out-of-stock" ><span>' . __('Fuera de stock', 'envy') . '</span></label>';
//    }
//}

add_action('woocommerce_single_product_summary', 'envy_stock_catalog');

// Register Custom Taxonomy
function custom_taxonomy()
{
    $labels = array(
        'name' => _x('Marcas', 'Taxonomy General Name', 'text_domain'),
        'singular_name' => _x('Marca', 'Taxonomy Singular Name', 'text_domain'),
        'menu_name' => __('Marcas', 'text_domain'),
        'all_items' => __('All Items', 'text_domain'),
        'parent_item' => __('Parent Item', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'new_item_name' => __('New Item Name', 'text_domain'),
        'add_new_item' => __('Add New Item', 'text_domain'),
        'edit_item' => __('Edit Item', 'text_domain'),
        'update_item' => __('Update Item', 'text_domain'),
        'view_item' => __('View Item', 'text_domain'),
        'separate_items_with_commas' => __('Separate items with commas', 'text_domain'),
        'add_or_remove_items' => __('Add or remove items', 'text_domain'),
        'choose_from_most_used' => __('Choose from the most used', 'text_domain'),
        'popular_items' => __('Popular Items', 'text_domain'),
        'search_items' => __('Search Items', 'text_domain'),
        'not_found' => __('Not Found', 'text_domain'),
        'no_terms' => __('No items', 'text_domain'),
        'items_list' => __('Items list', 'text_domain'),
        'items_list_navigation' => __('Items list navigation', 'text_domain'),
    );
    $rewrite = array(
        'slug' => 'marca',
        'with_front' => true,
        'hierarchical' => true,
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'rewrite' => $rewrite,
    );
    register_taxonomy('taxonomy', array('product'), $args);
}

add_action('init', 'custom_taxonomy', 0);

// Register Custom Taxonomy
function custom_taxonomy2()
{
    $labels = array(
        'name' => _x('Genero', 'Taxonomy General Name', 'text_domain'),
        'singular_name' => _x('Genero', 'Taxonomy Singular Name', 'text_domain'),
        'menu_name' => __('Genero', 'text_domain'),
        'all_items' => __('All Items', 'text_domain'),
        'parent_item' => __('Parent Item', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'new_item_name' => __('New Item Name', 'text_domain'),
        'add_new_item' => __('Add New Item', 'text_domain'),
        'edit_item' => __('Edit Item', 'text_domain'),
        'update_item' => __('Update Item', 'text_domain'),
        'view_item' => __('View Item', 'text_domain'),
        'separate_items_with_commas' => __('Separate items with commas', 'text_domain'),
        'add_or_remove_items' => __('Add or remove items', 'text_domain'),
        'choose_from_most_used' => __('Choose from the most used', 'text_domain'),
        'popular_items' => __('Popular Items', 'text_domain'),
        'search_items' => __('Search Items', 'text_domain'),
        'not_found' => __('Not Found', 'text_domain'),
        'no_terms' => __('No items', 'text_domain'),
        'items_list' => __('Items list', 'text_domain'),
        'items_list_navigation' => __('Items list navigation', 'text_domain'),
    );
    $rewrite = array(
        'slug' => 'genero',
        'with_front' => true,
        'hierarchical' => true,
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'rewrite' => $rewrite,
    );
    register_taxonomy('taxonomy2', array('product'), $args);
}

add_action('init', 'custom_taxonomy2', 0);

//add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 4;' ), 4 );
add_filter('woocommerce_product_tabs', 'woo_remove_product_tabs', 98);

function woo_remove_product_tabs($tabs)
{
    unset($tabs['additional_information']);    // Remove the additional information tab
    return $tabs;
}

// Muestra el porcentaje de descuento en el producto
add_filter('woocommerce_sale_flash', 'muestra_porcentaje_descuento', 10, 3);
//enganchar top-bar movil,a header
add_action('storefront_header', 'storefront_handheld_footer_bar', 999);
function muestra_porcentaje_descuento($text, $post, $product)
{
//    $variation_min_reg_price = $product->get_variation_regular_price('min', true);
//    $variation_max_reg_price = $product->get_variation_regular_price('max', true);
//    $variation_min_sale_price = $product->get_variation_sale_price('min', true);
//    $variation_max_sale_price = $product->get_variation_sale_price('max', true);

    if ($product->product_type == 'variable') {
        $regular_price = $product->get_variation_regular_price('max', true);
        $sale_price = $product->get_variation_sale_price('min', true);
    } else {
        $regular_price = $product->regular_price;
        $sale_price = $product->sale_price;
    }
    $percentage = round((($regular_price - $sale_price) / $regular_price) * 100);
    $text = __('Oferta ', 'woocommerce') . $percentage . '%';
    echo "<span class='onsale'>" . $text . '</span>';
}

function logout_redirect_home()
{
    wp_safe_redirect(home_url());
    exit;
}

add_action('wp_logout', 'logout_redirect_home');
function wpse101952_redirect()
{
    global $post;

    if (!is_user_logged_in()) { //examples: is_home() or is_single() or is_user_logged_in() or isset($_SESSION['some_var'])

        wp_safe_redirect(home_url());

        exit();
    }
}

add_action('mi-cuenta', 'wpse101952_redirect');

remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
//Ventas cruzarlas engancharlo a otro hook
add_action('woocommerce_after_cart_table', 'woocommerce_cross_sell_display');
//remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
//Quitar campos en finalizar compra
add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');
function custom_override_checkout_fields($fields)
{
    //  unset($fields['billing']['billing_first_name']);
//    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_company']);
    //  unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
//    unset($fields['billing']['billing_city']);
//    unset($fields['billing']['billing_postcode']);
    //  unset($fields['billing']['billing_country']);
    //  unset($fields['billing']['billing_state']);
//    unset($fields['billing']['billing_phone']);
//factura    unset($fields['order']['order_comments']);
    //  unset($fields['billing']['billing_address_2']);
    //  unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_company']);
//    unset($fields['billing']['billing_last_name']);
    //  unset($fields['billing']['billing_email']);
    //  unset($fields['billing']['billing_city']);
    return $fields;
}

/**
 * Display the theme credit
 *
 * @since  1.0.0
 * @return void
 */
function storefront_credit()
{
    ?>
    <div class="site-info">

    </div>
    <div class="copy">
        <?php echo esc_html(apply_filters('storefront_copyright_text', $content = '&copy; ' . get_bloginfo('name') . ' ' . date('Y'))); ?>
    </div>
    <div class="pagos">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/credit-cards/visa.svg" alt="visa"/>
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/credit-cards/mastercard.svg"
             alt="masterCard"/>
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/credit-cards/paypal.svg" alt="paypal"/>
    </div><!-- .site-info -->
    <?php
}

remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
remove_action('woocommerce_checkout_order_review', 'woocommerce_order_review', 10);
remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);

function logout_redirect_home2()
{
    echo apply_filters('woocommerce_order_button_html', '<input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Realizar Pedido" data-value="Realizar Pedido" />');
}

add_action('woocommerce_checkout_order_review', 'logout_redirect_home2', 30);
?>
<?php function registrar_sidebar()
{
    register_sidebar(array(
        'name' => 'Sidebar para el blog',
        'id' => 'sidebar-blog',
        'description' => 'sidebar para mostrar en el lateral blog',
        'class' => 'sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'registrar_sidebar'); ?>