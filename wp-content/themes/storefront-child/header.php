<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <meta name="author" content="David Lopez Fernandez">
    <link href="<?php echo get_home_url() ?>/favicon.ico" rel="icon" type="image/x-icon"/>
</head>

<body <?php body_class(); ?>>
<!--  <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">-->
<?php wp_head(); ?>

<div id="page" class="hfeed site">
    <div id="topBar">
        <div class="col-full">
            <div id="right">
                <?php
                function platzi_add_meta_description()
                {


                }

                add_action('storefront_before_header', 'platzi_add_meta_description');
                add_action('storefront_before_header', 'storefront_header_cart');

                //wp_nav_menu( array( 'theme_location' => 'footer' ) );

                do_action('storefront_before_header');
                if (is_user_logged_in()) {
                    global $current_user;
                    // Obtenemos la informacion del usuario conectado y asignamos los valores a las variables globales
                    // Mas info sobre 'get_currentuserinfo()':
                    // http://codex.wordpress.org/Function_Reference/get_currentuserinfo
                    get_currentuserinfo();
                    // Guardamos el nombre del usuario en una variable
                    $usuario = esc_attr($current_user->user_login); ?>
                    <span class="fa fa-user"><a class="mi-cuenta"
                                                href="<?php echo get_home_url() ?>/mi-cuenta"><?php echo $usuario ?></a></span>
                    <?php
                } else {
                    ?><span class="fa fa-user"><a class="login" href="<?php echo get_home_url() ?>/login">Mi cuenta</a>
                    </span>
                    <?php
                }
                ?>



                <?php
                ?>
            </div>
            <?php dynamic_sidebar('topleft'); ?>
        </div>
    </div>
    <header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">
        <div class="col-full">
            <?php
            /**
             * Functions hooked into storefront_header action.
             *
             * @hooked storefront_skip_links                       - 0
             * @hooked storefront_social_icons                     - 10
             * @hooked storefront_site_branding                    - 20
             * @hooked storefront_secondary_navigation             - 30
             * @hooked storefront_product_search                   - 40
             * @hooked storefront_primary_navigation_wrapper       - 42
             * @hooked storefront_primary_navigation               - 50
             * @hooked storefront_header_cart                      - 60
             * @hooked storefront_primary_navigation_wrapper_close - 68
             */
            do_action('storefront_header');
            echo do_shortcode('[aws_search_form]');
            ?>

        </div>
    </header>

    <!-- #masthead -->
    <?php
    /**
     * Functions hooked in to storefront_before_content.
     *
     * @hooked storefront_header_widget_region - 10
     */
    do_action('storefront_before_content');

    ?>
    <div id="content" class="site-content" tabindex="-1">
        <div class="col-full">
<?php
/**
 * Functions hooked in to storefront_content_top.
 *
 * @hooked woocommerce_breadcrumb - 10
 */
do_action('storefront_content_top');



