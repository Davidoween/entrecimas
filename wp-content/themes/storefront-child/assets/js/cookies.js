function getCookie(c_name){
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1){
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1){
        c_value = null;
    }else{
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1){
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start,c_end));
    }
    return c_value;
}

function setCookie(c_name,value,exdays){
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}

if(getCookie('entrecimas')!="1"){
	jQuery("body").prepend("<div class='msgcookie'><p><a href='#' class='close'></a>Esta web utiliza cookies. Si sigues navegando, entendemos que aceptas estas <a href='http://entrecimas.com/terminos-y-condiciones/'>condiciones de uso</a></p></div>");

	jQuery("body").on("click", ".close", function(e) {
		e.preventDefault();
	  setCookie('entrecimas','1',365);
		jQuery(".msgcookie").fadeOut();
	});
}
