<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 */
?>

</div><!-- .col-full -->
</div><!-- #content -->

<?php do_action('storefront_before_footer'); ?>
<aside class="pre-footer">
    <div class="col-full ">
        <div class="caja"><i class="fa fa-truck" aria-hidden="true"></i>
            <p>Envío rápido</p></div>
        <div class="caja"><i class="fa fa-lock" aria-hidden="true"></i>
            <p>Pago seguro</p></div>
        <div class="caja"><i class="fa fa-tags" aria-hidden="true"></i>
            <p>Primeras marcas</p></div>
        <div class="caja"><i class="fa fa-exchange" aria-hidden="true"></i>
            <p>Devoluciones e intercambios gratuitos</p></div>
    </div>
</aside>
<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="col-full">

        <?php
        /**
         * Functions hooked in to storefront_footer action.
         *
         * @hooked storefront_footer_widgets - 10
         * @hooked storefront_credit         - 20
         */
        do_action('storefront_footer'); ?>

    </div><!-- .col-full -->

    <div id="top" class="hide">
        <i class="fa fa-arrow-up" aria-hidden="true"></i>
    </div>
</footer><!-- #colophon -->

<?php do_action('storefront_after_footer'); ?>

</div><!-- #page -->


<script type="text/javascript">
    jQuery(document).on('click', '.menu-item-has-children>a', function (event) {
        if (jQuery(window).width() < 768) {
            event.preventDefault();
            d = jQuery(this).siblings('ul').children('li');
            displaying = d.css("max-height");
            if (displaying == "0px") {
                jQuery(d).css('max-height', '1000px');
            } else {
                jQuery(d).css('max-height', '0px');
            }
        }

    });
</script>
<script type="text/javascript">
    jQuery("#top").mousedown(function () {
        jQuery("html, body").animate({
            scrollTop: 0
        }, 1000);
        return false;
    });
</script>

<script type="text/javascript">
    jQuery("#filtro").click(function () {
        jQuery('html,body').animate({
            scrollTop: jQuery(".woof_redraw_zone").offset().top - 50
        }, 2000);
    });
</script>


<script type="text/javascript">
    jQuery(document).on("woof_ajax_done", woof_ajax_done_handler);
    function woof_ajax_done_handler(e) {
        jQuery("html, body").animate({
            scrollTop: 0
        }, 1000);
        return false;
    }
</script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {

        jQuery('.cart_item').on('click', 'a', function (e) {
            $(this).parents('tr').remove();
        });
        jQuery('body').on('change', 'input[type=number]', function (e) {
            $('input[name="update_cart"]').trigger("click");

        });
        jQuery('body').on('click', '.plus', function (e) {
            $input = jQuery(this).prev('input.qty'); //valor de cantidad
            var precioConEuro = jQuery('.product-price').children('.amount').text();
            precio = parseInt(precioConEuro.substring(0, precioConEuro.length - 1));
            var val = parseInt($input.val()); //valor cantidad

            max = $input.attr('max');
            if (max != "") {
                if (val >= max) {
                    alert("Solo tenemos " + max + " unidades. Háblamos por el chat para mas información");
                    return false;
                }
            }

            $input.val(val + 1).change(); //cantidad +1 y lo cambiamos
//            jQuery('td[data-title="Total"]').text(((val + 1) * precio) + "€");
//            jQuery('td[data-title="Subtotal"]').text(((val + 1) * precio) + "€");
//            jQuery('td[data-title="Total"]').text(((val + 1) * precio) + "€");
            $('input[name="update_cart"]').trigger("click");
        });

        $('body').on('click', '.minus',
            function (e) {
                $input = jQuery(this).next('input.qty');
                var val = parseInt($input.val());
                if (val > 1) {
                    $input.val(val - 1).change();
                    var precioConEuro = jQuery('.product-price').children('.amount').text();//precio €
                    precio = parseInt(precioConEuro.substring(0, precioConEuro.length - 1));//precio
//                    jQuery('td[data-title="Total"]').text(((val - 1) * precio) + "€");
//                    jQuery('td[data-title="Subtotal"]').text(((val - 1) * precio) + "€");
//                    jQuery('td[data-title="Total"]').text(((val - 1) * precio) + "€");
                    $('input[name="update_cart"]').trigger("click");
                }
            });
    });
</script>

<script type="text/javascript">


    jQuery(document).ready(function ($) {

        var myScrollFunc = function () {
            var y = window.scrollY;
            if (y >= 500) {
                $('#top').fadeIn();
            } else {
                $('#top').fadeOut();
            }
        };

        window.addEventListener("scroll", myScrollFunc);
    });

</script>
<?php wp_footer(); ?>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-89481632-1', 'auto');
    ga('send', 'pageview');

</script>
<script>
    jQuery(document).ready(function ($) {

        jQuery('body').on('click', '.checkout-button,.single_add_to_cart_button', function () {
            if ($(this).hasClass('single_add_to_cart_button')) {
                input = $('input[type=number]');
                if (input.val() > input.attr('max')) {
                    return;
                }
            }
            if (!$(this).hasClass('disabled')) {
                $(this).html('<img class="spinner" src="<?php echo get_home_url(); ?>/wp-content/themes/storefront-child/assets/loading-spin.svg" /> &nbsp; Espere ...')
            }

        });
    });

</script>
<script type="text/javascript"
        src="<?php echo get_home_url(); ?>/wp-content/themes/storefront-child/assets/js/cookies.js"></script>
</body>
</html>
