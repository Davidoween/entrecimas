<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: Template login
 */
?>
 <!DOCTYPE html>
 <html <?php language_attributes(); ?>>
 <head>
   <title>Login Entrecimas</title>
   <meta charset="<?php bloginfo('charset'); ?>">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
   <link rel='stylesheet' id='storefront-style-css'  href='<?php  echo get_home_url();?>/wp-content/themes/storefront/style.css?ver=2.1.6' type='text/css' media='all' />
   <link rel='stylesheet' id='storefront-style-css'  href='<?php echo get_home_url();?>/wp-content/themes/storefront-child/style.css' type='text/css' media='all' />
   <?php wp_head(); ?>
 </head>
 	<header id="login">
 		<a href="<?php echo get_home_url();  ?>">
      <div>
    	<img id="img-logo" src="<?php echo get_home_url();  ?>/wp-content/uploads/logo/logoLogin2.png" alt="" />
      <h1>EntreCimas</h1>
    </div>
    </a>
 	</header>
 			<?php while (have_posts()) : the_post();
                do_action('storefront_page_before');
                get_template_part('content', 'page');
                /*
                 * Functions hooked in to storefront_page_after action
                 *
                 * @hooked storefront_display_comments - 10
                 */
                do_action('storefront_page_after');

            endwhile; // End of the loop.?>

    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
