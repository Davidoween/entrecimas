<?php if ( is_active_sidebar( 'sidebar-blog' ) ) : ?>
    <div id="widget-area" class="widget-area">
        <?php dynamic_sidebar( 'sidebar-blog' ); ?>
    </div>
<?php endif; ?>
