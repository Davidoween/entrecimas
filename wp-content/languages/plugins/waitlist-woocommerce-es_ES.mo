��          �      L      �     �     �     �     �       !        <  1   [     �     �  �   �  `   F  .   �     �  L   �  '   @  O   h     �  �  �      m     �     �     �      �  (   �  &     <   +     h     q  �   z  �   5  (   �  "   �  `   		  0   j	  Z   �	      �	                                      	                                         
                     %s is back in stock BUY NOW Email Email address cannot be empty. Email me when available Please enter valid email address. Please verfiy you are a human. Product is already in stock. You can add to cart. Quantity Quantity required We are extremely pleased to announce that the product is now avaiable for purchase. Please act fast, as the item may only be available in limited quantities. We will inform you when the product arrives in stock. Just leave your valid email address below. We won't share your address with anybody else. You are already in waitlist. You are now in waitlist. We will inform you as soon as we are back in stock. You need to Login for joining waitlist. You requested to be notified when %s was back in stock and available for order. Your Product is Now In Stock. Project-Id-Version: Waitlist WooCommerce
POT-Creation-Date: 2017-01-12 19:35+0100
PO-Revision-Date: 2017-01-12 20:55+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
Last-Translator: 
Language: es
X-Poedit-SearchPath-0: .
 %s producto está ahora en Stock Comprar Email El email no puede estar vacio Avísame cuando esté disponible Por favor, escribe una dirección valida Por favor, verifica que eres un humano El producto ya está en Stock. Puedes añadirlo a tu carrito Cantidad Cantidad Estamos muy contentos de anunciar que el producto ya está disponible para su compra. Por favor actúe rápido, ya que el artículo sólo se encuentra disponible en cantidades limitadas. Le notificaremos al correo cuando este producto esté disponible. Simplemente deje su dirección de correo electrónico a continuación No compartiremos su dirección con nadie Tu ya estás en la lista de espera Gracias, te has añadido a la lista de espera. Le informaremos cuando el producto esté en Stock Necesitas iniciar sesión para unirte a la lista Usted solicitó ser avisado, cuando %s volviera a estar en stock y disponible para comprar Tu producto está ahora en Stock 