��    '      T  5   �      `  $   a     �     �     �  	   �  @   �            (        D  6   L  #   �  k   �          $     0  
   7     B  l   K  �   �     s     �     �  #   �     �  
   �     �  
          .   $  h   S     �     �  	   �     �  !   �  
           :  8  2   s	     �	     �	     �	     �	  d   
     k
     s
  :   �
  	   �
  >   �
  $     k   -  $   �     �     �     �     �  �     �   �  (   e     �     �  2   �  !   �               (     5  .   N  s   }     �  )   
     4     H  )   \  	   �     �           	      '         %   !                
      #                                                                                                          "          &   $        Override Saved Tab for this product  before activating the plugin. Add Tab Add a Saved Tab Add a Tab Any updates made here will apply to all products using this tab. Apply Bulk Actions Check this box to override the saved tab Content Create and save tabs you can add to multiple products. Custom Product Tabs for WooCommerce Custom Product Tabs for WooCommerce could not be activated because WooCommerce is not installed and active. Custom Tab Title Custom Tabs Delete Delete Tab Edit Tab Extend WooCommerce to add and manage custom product tabs. Create as many product tabs as needed per product. For help using Custom Tabs please visit our <a href='https://yikesplugins.com/support/knowledge-base/product/easy-custom-product-tabs-for-woocommerce/' target='_blank'>Knowledge Base</a> Go Back to Saved Tabs list HTML and text to display. Help Me! Override Saved Tab for this product Please install and activate  Remove Tab Save Tab Select All Select bulk action Settings - Custom Product Tabs for WooCommerce Sorry! An error has occurred while trying to retrieve the editor. Please refresh the page and try again. Tab Content Tab Content Preview Tab Title Tab deleted! There are no saved tabs. Add one! YIKES, Inc http://www.yikesinc.com PO-Revision-Date: 2017-01-29 12:32:25+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Plugins - Custom Product Tabs for WooCommerce - Stable (latest release)
 Sobreescribir la pestaña guardada a este producto antes de activar el plugin. Añadir pestaña Añadir una pestaña guardada Añadir una pestaña Cualquier modificación que se haga aquí se aplicará a todos los productos que usen esta pestaña. Aplicar Acciones en lote Marca esta casilla para sobreescribir la pestaña guardada Contenido Crea y guarda pestañas que puedes añadir a varios productos. Custom Product Tabs para WooCommerce Custom Product Tabs para WooCommerce no ha podido activarse ya que WooCommerce no está instalado y activo. Título de la pestaña personalizada Pestañas personalizadas Borrar Borrar pestaña Modificar pestaña Amplía WooCommerce para poder añadir y gestionar pestañas personalizadas de producto. Crea tantas pestañas de producto como necesites. Para ayuda sobre cómo usar pestañas personalizadas visita nuestra <a href='https://yikesplugins.com/support/knowledge-base/product/easy-custom-product-tabs-for-woocommerce/' target='_blank'>base de conocimiento</a> Volver a la lista de pestañas guardadas HTML y texto a mostrar. ¡Ayúdame! Sobreescribir la pestaña guardada a este producto Por favor, instálalo y actívalo Quitar pestaña Guardar pestaña Elegir todas Elige la acción en lote Ajustes - Custom Product Tabs para WooCommerce ¡Lo siento! Ocurrió un error al tratar de mostrar el editor. Por favor, recarga la página e inténtalo de nuevo. Contenido de la pestaña Vista previa del contenido de la pestaña Título de pestaña ¡Pestaña borrada! No hay pestañas guardadas. ¡Añade una! YIKES Inc http://www.yikesinc.com 